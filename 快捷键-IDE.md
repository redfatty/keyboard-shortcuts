

### IDE快捷键说明
- 顺序: `IDEA` `Eclipse` `Xcode`
- Delete: `Del`
- Backspace: `Bksp`
- PageUp: `PgUp`
- PageDonw: `PgDn`
- 上下左右: `← →  ↑ ↓ `

### 相同快捷键, 不同操作
- `Ctrl+D`
  - Idea: 向下复制行
  - Eclipse: 删除行
- `Ctrl+Y`
  - Idea: 删除行
  - Eclipse: 反撤消
- `Ctrl+W`
  - Idea: 选中行,代码块等
  - Eclipse: 关闭标签



### 文件操作
- 显示文件目录: `标签上悬停`
- 显示文件目录: `Ctrl+单击文件窗口标签, 单击目录可打开`
- 重命名: `Shift+F6` `Alt+Shift+R`

### 搜索
- 任意搜索: `Shift+Shift`
- 当前文件:
  - 搜索: `Ctrl+F`
  - 搜索替换: `Ctrl+R`

### 窗口
- 横向滚动: `Shift+鼠标滑轮` `Eclipse`
- 终端: `Alt+F12` `eclipse`
- 关闭当前标签: `Ctrl+F4` `Ctrl+W`
- 关闭所有标签: ` idea ` `Ctrl+Shift+W`
- Idea关闭某个标签: `Shift+单击某个标签` 
- 标签切换: `Alt+ ←/→` `eclipse`
- 方法之间切换: `Alt+ ↑/↓`

### 文本通用操作
- 撤消: `Ctrl+Z` `Ctrl+Z` 
- 反撤消: `Ctrl+Shift+Z` `Ctrl+Y`

### 文本删除
- 删除当前行: `Ctrl+Y` `Ctrl+D`
- 剪切当前行: `Ctrl+X`
- 删除单词: `Ctrl+退格/Delete`
- 删除下一个换行: `Ctrl+Shift+J` `Eclipse`

### 文本选中
- 按单词选中: `Ctrl+Shift+Left/Right`
- Idea选中当前单词/行/方法体/代码块等: `Ctrl+W`
- Eclipse选中当前行: `先Ctrl+D再Ctrl+Z`
  
### 文本复制
- 复制当前行: `Ctrl+C`
- 向上复制行: `无` `Ctrl+Alt+Up`   
- 向下复制行: `Ctrl+D` `Ctrl+Alt+Dn` 
- 复制文件路径: `Ctrl+Shift+C`
- 复制全类/包名: `Ctrl+Alt+Shift+C`

### 文本移动
- Eclipse上下移动所在行或选中行: `Alt+Up/Dn`
- Idea上下移动所在行: `Alt+Shift+Up/Dn` 
- Idea上下移动选中行: `Ctrl+Shift+Up/Dn 每行的非空白字符都要选中`  
- Idea上下移动方法体: `Ctrl+Shift+Up/Dn 光标位于方法签名上或结尾处`

### 文本转换
- 大小写转换: `Ctrl+Shift+U` `Ctrl+Shift+X/Y`

### 光标移动
- 按单词移动: `Ctrl+Left/Right`
- 往上插一行: `Ctrl+Alt+Enter` `Ctrl+Shift+Enter`
- 往下插一行: `Shift+Enter` `Shift+Enter`
- 历史编辑位置跳转: `Ctrl+Alt+ ←/→` `Alt+ ←/→`
- 方法跳转: ```F2, 加Shift向上跳转```
    - 注意: `F2`在有错误时将在错误之间跳转

### 代码修复
- 优化导包: `Ctrl+Alt+O` `Ctrl+Shift+O` 
- 代码生成: `Alt+Insert` `Alt+Shift+R` ` `
- 实现方法: `Ctrl+I`
- 修复提示: `Alt+Enter` `Ctrl+1`
- 代码格式化: `Ctrl+Alt+L` `Ctrl+Shift+F`

### 注释

- 注释所在行或选中行: `i` `C`
- Idea  
    - 单行注释: `Ctrl+/`
    - 多行注释: `Ctrl+Sifth+/`
- Eclipse:
    - 注释或反注释: `Ctrl+Shift+C 或者 Ctrl+ /`
    - xml文件中: `Ctrl+Shift+C 或者 Ctrl+Shift+/及\`
  