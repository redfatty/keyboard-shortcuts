菜单窗口中:
Alt + 下划线字母 : 选择该选项



- 显示任务栏: win + B
- ctrl + O : 查看所有方法, 按两次显示父类所有方法
- shift + ctrl + T : 搜索类型
- ctrl + W : 关闭标签
- Shift + Ctrl + W :关闭所有标签
- Ctrl + S : 保存
- Shift + Ctrl + S : 保存全部
- /** + Enter : 添加方法注释
- Shift + Ctrl + R : 搜索文件
- Shift + Alt + R : 重命名文件
- Ctrl + Enter : 往下打开一空行
- Shift + Ctrl + Enter : 往上打开一空行

### 运行/调式
- 运行
    - Ctrl + F11

→←↑↓

###
Alt + F11 : 最大化Eclipse窗口切换



### 透视图窗口
Ctrl + M : 最大化切换
Ctrl + E : 快速切换标签窗口

### 注释
- 所在行/选中行
    - Ctrl + / : 注释或反注释Java代码
    - Ctrl + Shift + C : 注释或反注释Java/xml/properties等。
- 选中内容
    - Ctrl + Shift + / : 注释
    - Ctrl + Shift + \ : 反注释
    - XML文件中只能用这种方式注释
- 类/方法
    - /** + Enter
    - Alt + Shift + J
        - 光标在方法内部,就添加方法注释
        - 光标不在方法内部, 就给类添加注释


### 搜索
- Ctrl + F
    - Enter 往下找结果  
    - Ctrl + Enter + 往上找结果
- Ctrl + J : 增量查找, 非常实用
    - Ctrl + J : 往下找
    - Ctrl + Shift + J : 往上找
- Ctrl + K : 搜索当前选中的文本
- 快速访问
    - F3
- Ctrl + T: 查看继承树
- 选中类名+F4, 查看继承树, 包括自定义的类

### 文本编辑
- Ctrl + Z : 撤消
- Ctrl + Y : 反撤消
- Ctrl + Shift + F : 代码格式化
- 复制



### 选择文本
- 块选择切换
    - Alt + Shift + A
    - 点击工具栏中类似梯子图片

- ???
    - Alt + Shift + → ← ↑ ↓
        - 快捷键表中是加下是复原上一个选择.
        - ??好像是会判断哪些内容为一个整块,再进行选择或反选择

### 光标跳转
- Ctrl + Shift + ↑ ↓
    - Java中,跳到下一个类成员上(成员变量,方法等)
    - XML中, 跳到下一个元素上.
- 代码跳转
    - F3
    - Ctrl + 单击

### 文本显示
- Alt + Shift + Y: 自动换行切换

### 各种快速修正
- 选择的代码抽取方法
- 包导入修复: Ctrl + Shift + O
- 代码块包围: Alt + Shift + Z, 包括if,try等

### 内容提示
- Alt + /
    - 方法重写提示(光标在方法体之外)
    - XML中可以提示当前光标处可以添加什么标签
- Ctrl + Alt + /
    - 直接补全内容,而不提示



### 文件/包操作
- 移动
    - Alt + Shift + V
- 重命名
    - Alt + Shift + R


